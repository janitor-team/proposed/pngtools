pngtools (0.5~git20220314.1ccca3a-2) unstable; urgency=medium

  [ Michael Schaller ]
  * d/patches: Package fails to build without network access. Closes: 1013909

  [ Mathieu Malaterre ]
  * d/control: Bump Std-Vers to 4.6.1 no changes needed

 -- Mathieu Malaterre <malat@debian.org>  Thu, 11 Aug 2022 14:51:55 +0200

pngtools (0.5~git20220314.1ccca3a-1) unstable; urgency=medium

  * New upstream version 0.5~git20220314.1ccca3a

 -- Mathieu Malaterre <malat@debian.org>  Wed, 30 Mar 2022 16:33:55 +0200

pngtools (0.5~git20220111.bebd7b3-1) unstable; urgency=medium

  * New upstream version 0.5~git20220111.bebd7b3
  * d/patches: Remove patches applied upstream

 -- Mathieu Malaterre <malat@debian.org>  Tue, 11 Jan 2022 11:52:03 +0100

pngtools (0.5~git20211218.2f4c05c-1) unstable; urgency=medium

  * d/control: Update to new Vcs URLs
  * d/rules: Make sure to compile without error
  * New upstream version 0.5~git20211218.2f4c05c
  * d/patches: Import pull requests from upstream repo. Closes: #544489, #668436, #743279
  * d/rules: Properly generate man page from upstream SGML

 -- Mathieu Malaterre <malat@debian.org>  Mon, 03 Jan 2022 12:31:24 +0100

pngtools (0.5~git20120106.409eb16-1) unstable; urgency=medium

  * d/watch: Adapt watch until upstream publish tags
  * New upstream version 0.5~git20120106.409eb16
  * d/control: Move to team maintenance (phototools). Closes: #863326
  * d/patches: Remove patches applied upstream

 -- Mathieu Malaterre <malat@debian.org>  Fri, 03 Dec 2021 14:42:40 +0100

pngtools (0.4-2) unstable; urgency=medium

  * QA upload.
  * Refresh packaging:
    + Set package maintainer to Debian QA Group.
    + Convert to "3.0 (quilt)" format.
    + Bump Standards-Version to 4.6.0.
    + Bump debhelper compat to v13.
    + Add Vcs-* fields in debian/control file.
    + Monitor new upstream on GitHub.

 -- Boyuan Yang <byang@debian.org>  Tue, 12 Oct 2021 15:41:33 -0400

pngtools (0.4-1.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Switch to autoreconf (Closes: #822547)
  * Bump compat level to 9
  * Drop cdbs
  * Refactor rules file, fixing a build failure
    with as-needed flags.
  * Patch configure.in following png rename of
    api.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sun, 24 Apr 2016 22:47:36 +0200

pngtools (0.4-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix previous upload for libpng12 by conditional compiling (Closes: #811001)

 -- Tobias Frost <tobi@debian.org>  Thu, 14 Jan 2016 20:40:19 +0100

pngtools (0.4-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "FTBFS with libpng 1.5", taking patch from Nobuhiro (Closes: #641892)
  * Change B-D to libpng-dev from libpng12-dev (Closes: #662476)

 -- Tobias Frost <tobi@debian.org>  Thu, 07 Jan 2016 01:05:36 +0100

pngtools (0.4-1) unstable; urgency=low

  * New upstream release;
  * Update compat level to 7;
  * Update Standards-Version to 3.8.3:
    - Using new Homepage field in debian/control.
  * Remove patches/pngchunks.diff (applied upstream);
  * Fix minus sign in manpages.

 -- Nelson A. de Oliveira <naoliv@debian.org>  Tue, 18 Aug 2009 17:04:10 -0300

pngtools (0.3-2) unstable; urgency=low

  * Fix pngchunks on 64 bits platforms (Closes: #503820).

 -- Nelson A. de Oliveira <naoliv@debian.org>  Wed, 10 Dec 2008 08:06:18 -0200

pngtools (0.3-1) unstable; urgency=low

  * Initial release (Closes: #405139).

 -- Nelson A. de Oliveira <naoliv@debian.org>  Tue, 31 Jul 2007 17:45:22 -0300
